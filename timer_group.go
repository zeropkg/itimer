package itimer

import (
	"sync"
	"time"
)

type TimeUsed struct {
	Key   string
	Value int64
}

type ITimeGroup interface {
	TimeStart(keys ...string)
	TimeStop(keys ...string)
	CostTime(keys ...string) func()
	GetTime(key string) time.Duration
	GetAllTime() map[string]time.Duration
}

type TimeGroupImpl struct {
	sync.Map
	Precision     int      // 时间精度
	Sequence      []string // 顺序， 主要是确定start的调用顺序
	DisableStatus bool     // 是否禁用
}

func NewTimerGroup(precisions ...int) ITimeGroup {
	tg := TimeGroupImpl{
		Precision: getPrecision(precisions...),
		Sequence:  []string{},
	}
	return &tg
}

func (tg *TimeGroupImpl) Enable() {
	tg.DisableStatus = false
}

func (tg *TimeGroupImpl) Disable() {
	tg.DisableStatus = true
}

func (tg *TimeGroupImpl) IsDisable() bool {
	return tg.DisableStatus
}

func (tg *TimeGroupImpl) TimeStart(keys ...string) {
	if tg == nil || tg.IsDisable() {
		return
	}

	key := getKey(keys...)
	// 如果已经存在，do nothing
	if tg.exists(key) {
		return
	}
	defer tg.appendSequence(key)

	t := newTimer()
	t.start()

	tg.Store(key, t)

	return
}

func (tg *TimeGroupImpl) TimeStop(keys ...string) {
	if tg == nil || tg.IsDisable() {
		return
	}

	key := getKey(keys...)
	value, exists := tg.Load(key)

	if !exists {
		// 如果不存在，do nothing
		return
	}

	t, convertOk := value.(*Timer)

	if convertOk {
		// 如果转换成功就stop
		t.stop()
	}

}

// CostTime
func (tg *TimeGroupImpl) CostTime(keys ...string) func() {
	emptyFunc := func() {}
	if tg == nil || tg.IsDisable() {
		// 空方法
		return emptyFunc
	}
	key := getKey(keys...)
	defer tg.appendSequence(key)
	t := newTimer()
	tg.Store(key, t)
	f := t.costTime()
	return f
}

func (tg *TimeGroupImpl) GetTime(key string) (cost time.Duration) {
	if tg == nil {
		return
	}
	if tg.IsDisable() {
		return
	}

	value, exists := tg.Load(key)
	if !exists {
		// 如果不存在，do nothing
		return
	}

	t, convertOk := value.(*Timer)
	if !convertOk {
		return
	}

	return t.timeUsed
}

func (tg *TimeGroupImpl) GetAllTime() map[string]time.Duration {
	if tg == nil {
		return nil
	}
	if tg.IsDisable() {
		return nil
	}

	m := make(map[string]time.Duration)
	for _, key := range tg.Sequence {
		value, exists := tg.Load(key)
		if !exists {
			continue
		}
		t, convertOk := value.(*Timer)
		if !convertOk {
			continue
		}
		m[key] = t.timeUsed
	}
	return m
}

func (tg *TimeGroupImpl) TimeLog() (result string) {
	if tg == nil {
		return
	}
	if tg.IsDisable() {
		return
	}

	result = " "
	for _, key := range tg.Sequence {
		cost := tg.GetTime(key).String()
		result += "[" + key + "]" + cost + " "
		// result += key + "_" + timePostfix + "=" + strconv.FormatInt(cost, 10) + " "
	}

	return

}

////////////////////////////////////////////////////////////////////////////

func (tg *TimeGroupImpl) exists(key string) bool {
	_, ok := tg.Load(key)
	return ok
}

func (tg *TimeGroupImpl) appendSequence(key string) {
	if !containInStringSlice(tg.Sequence, key) {
		tg.Sequence = append(tg.Sequence, key)
	}
}

// func (tg *TimerGroup) timePostfix() string {
// 	switch tg.Precision {
// 	case PRECISION_MS:
// 		return "ms"
// 	case PRECISION_S:
// 		return "s"
// 	case PRECISION_US:
// 		return "us"
// 	default:
// 		return "ms"

// 	}
// }
