package itimer

import (
	"runtime"
	"strings"
)

func containInStringSlice(source []string, find string) bool {
	for _, item := range source {
		if item == find {
			return true
		}
	}
	return false
}

func getPrecision(precisions ...int) int {
	precision := DEFAULT_PRECISION
	if len(precisions) > 0 {
		precision = precisions[0]
	}
	return precision
}

func getKey(keys ...string) string {
	var key string
	if len(keys) > 0 {
		key = keys[0]
	} else {
		key = getCallerFuncName()
	}
	return key
}

func getCallerFuncName() string {
	pc, _, _, _ := runtime.Caller(FUNC_STACK_LEVEL)
	funcFullName := runtime.FuncForPC(pc).Name()
	slashFuncName := getLastStrByChar(funcFullName, "/")
	// 就a.b的形式，不需要去调用a. ，仅仅留下b。 选择困难症状
	funcName := getLastStrByChar(slashFuncName, ".")
	//funcName := getFunctionName(funcFullName)[4:]
	return funcName
}

func getLastStrByChar(source string, char string) string {
	index := len(source)
	if strings.HasSuffix(source, char) {
		// 如果原始字符串中以分隔字符串结尾，那么对原始字符串做归一化操作
		index = index - 1
		source = source[:index]
	}

	for ; index > 0; index-- {
		if char == source[index-1:index] {
			return source[index:]
		}
	}
	// 没找到的话就返回原始字符串
	return source
}

func getFunctionName(fn string) string {
	seps := []rune{'/', '.'}
	// 用 seps 进行分割
	fields := strings.FieldsFunc(fn, func(sep rune) bool {
		for _, s := range seps {
			if sep == s {
				return true
			}
		}
		return false
	})

	if size := len(fields); size > 0 {
		return fields[size-1]
	}
	return ""
}
