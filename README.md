# timer

> 这是用于时间cost的底层库，你可以用它堆栈式的记录方法的耗时，最后调用TimeLog方法获取全部的time cost string。

默认时间精度

> 毫秒

方法列表

> ~~~go
> NewTimerGroup() // 构造方法
> 
> Start() // 启动计时器
> 
> Stop() // 暂停计时器
> 
> CostTime() // 轻量级计时用法，调用的时候需要 defer CostTime()()
> 
> TimeLog() // 格式化输出所有的计时器log string
> 
> GetTime(key string) //  返回计时时间int64
> 
> Disable()  // 禁用当前的计时器，属于TimerGroup对象级别的开关控制
> 
> Enable()  // 启用当前的计时器,默认是开启的
> 
> IsDisable() // 查询当前计时器的开关状态
> ~~~
>
> 

常规用法

> ```go
> func main() {
> 	tg := itimer.NewTimerGroup()
> 	tg.Start("随便指定的字符串") // 指定了key
> 	time.Sleep(time.Second * 2)
> 	tg.Stop("随便指定的字符串") // stop要和start成对儿存在
> 
> 	tg.Start() // 未指定key，那么会默认调用的方法
> 	time.Sleep(time.Second * 1)
> 	tg.Stop() // stop要和start成对儿存在
> 
> 	fmt.Println(tg.TimeLog())
> }
> ```
>
> 输出
>
> `随便指定的字符串_ms=2004 main_ms=1005`

轻量级用法

> ~~~go
> var tg = itimer.NewTimerGroup()
> 
> func BusinessFunc1() {
> 	// 1、要defer 
> 	// 2、CostTime()返回的是一个方法，要显示调用，所以是两个()
> 	defer tg.CostTime()() 
> 	time.Sleep(time.Second * 2)
> }
> 
> func BusinessFunc2() {
> 	defer tg.CostTime()() // 1、要defer 2、CostTime()返回的是一个方法，要显示调用，所以是两个()
> 	time.Sleep(time.Second * 2)
> }
> 
> func main() {
> 	BusinessFunc1()
> 	BusinessFunc2()
> 	fmt.Println(tg.TimeLog())
> }
> ~~~
>
> 输出
>
> `BusinessFunc1_ms=2003 BusinessFunc2_ms=2004`

另外，对于业务代码，可以把timer.TimerGroup类型自定义到struct中进行封装，也可以结合context Set() Get()进行TimerGroup对象的传递。



# 原理

>1. 类似下面这样返回方法指针，上层业务方法里面defer调用堆栈，退出时候执行这个方法指针方程一个计时器。
>
>~~~go
>func (t *Timer) costTime() func(){
>	t.start()
>	return func() {
>		t.stop()
>	}
>}
>~~~
>
>2. runtime.Callers 返回具体的方法调用栈，再用runtime.FuncForPC明确具体的调用的业务方法名称
>3. 工程上，底层实现timer逻辑，上层再封装层timerGroup，timerGroup主要包括一个数组一个map。数组用来保证计时的有序性；map每个kvpair，key表示计时的命名string，value表示一个确切的timer