package itimer

import (
	"encoding/json"
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/stretchr/testify/assert"
)

func TestGetTime0(t *testing.T) {
	tg := NewTimerGroup()
	tg.TimeStart("a")
	time.Sleep(time.Millisecond * 133)
	tg.TimeStop("a")
	spew.Dump(tg.GetTime("a").String())
}

func TestGetTime1(t *testing.T) {
	tg := NewTimerGroup()
	// 营造环境
	tg.TimeStart("tt")
	time.Sleep(time.Second * 2)
	tg.TimeStop("tt")

	tg.TimeStart()
	time.Sleep(time.Second * 1)
	tg.TimeStop()

	result := tg.GetTime("TestGetTime1")
	assert.GreaterOrEqual(t, result, int64(1000))
	spew.Dump(result)
}

func TestTimeLog1(t *testing.T) {
	tg := NewTimerGroup()
	//tg.Disable()
	// 营造环境
	tg.TimeStart("tt")
	time.Sleep(time.Second * 2)
	tg.TimeStop("tt")

	tg.TimeStart()
	time.Sleep(time.Second * 1)
	tg.TimeStop()

	fmt.Println(tg.(*TimeGroupImpl).TimeLog())
}

func TestGetLastStrByChar(t *testing.T) {
	str := "gitlab.luojilab.com/chenzhenyu/timer.TestTimeLog1"
	actual := getLastStrByChar(str, ".")
	assert.Equal(t, "TestTimeLog1", actual)
}

func TestStartWhenDisable(t *testing.T) {
	tg := NewTimerGroup()
	tg.(*TimeGroupImpl).Disable()
	//tg.Enable()

	tg.TimeStart()
	time.Sleep(time.Second * 1)
	tg.TimeStop()

	fmt.Println(tg.(*TimeGroupImpl).TimeLog())

}

func BenchmarkCommon(b *testing.B) {
	tg := NewTimerGroup()
	wg := sync.WaitGroup{}
	wg.Add(b.N)
	f := func(i int, waitGroup *sync.WaitGroup) {
		name := fmt.Sprintf("index%v", i)
		tg.TimeStart(name)
		time.Sleep(time.Second)
		tg.TimeStop(name)
		waitGroup.Done()
	}
	for i := 0; i < b.N; i++ {
		go f(i, &wg)
	}
	wg.Wait()
	fmt.Println(tg.(*TimeGroupImpl).TimeLog())
}

func BenchmarkCommon1(b *testing.B) {
	tg := NewTimerGroup()
	wg := sync.WaitGroup{}
	wg.Add(b.N)
	f := func(i int, waitGroup *sync.WaitGroup) {
		defer tg.CostTime()()
		time.Sleep(time.Second)
		waitGroup.Done()
	}
	for i := 0; i < b.N; i++ {
		go f(i, &wg)
	}
	wg.Wait()
	fmt.Println(tg.(*TimeGroupImpl).TimeLog())
}

func TestGroupAsyncCondition(t *testing.T) {
	tg := NewTimerGroup()
	// 营造环境
	tg.TimeStart("tt1")
	tg.TimeStart("tt2")

	bytes, _ := json.Marshal(tg)

	time.Sleep(time.Second * 2)
	json.Unmarshal(bytes, &tg)

	tg.TimeStop("tt1")
	tg.TimeStop("tt2")

	spew.Dump(tg.(*TimeGroupImpl).TimeLog())

}

func TestDemoX(t *testing.T) {
	type X struct {
		Flag string
		ITimeGroup
	}
	x := &X{
		"this is x",
		NewTimerGroup(),
	}

	bytes, _ := json.Marshal(x)
	spew.Dump(string(bytes))
	json.Unmarshal(bytes, &x)
	spew.Dump(x)
}
