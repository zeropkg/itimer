package itimer

// 时间精度
const (
	PRECISION_MS = 1
	PRECISION_S  = 2
	PRECISION_US = 3
	// 默认是毫秒的精度
	DEFAULT_PRECISION = PRECISION_MS
)

const (
	// 方法的堆栈层级
	FUNC_STACK_LEVEL = 3
)
