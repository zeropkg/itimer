package itimer

import (
	"time"
)

type Timer struct {
	startFlag bool
	stopFlag  bool
	beginTime time.Time     // 计时器的起始时间
	timeUsed  time.Duration // 计时器的耗时时间
}

func newTimer() *Timer {
	t := Timer{
		startFlag: false,
		stopFlag:  false,
	}
	return &t
}

// start 启动计时器
func (t *Timer) start() {
	if t.startFlag {
		return
	}
	t.startFlag, t.stopFlag = true, false
	t.beginTime = time.Now()
}

// stop  暂停计时器
func (t *Timer) stop() {
	// already stop
	if t.stopFlag {
		return
	}
	t.stopFlag = true
	t.timeUsed = time.Now().Sub(t.beginTime)
}

// costTime 轻量级计算时间耗时，但是有一些限制
func (t *Timer) costTime() func() {
	if !t.startFlag {
		t.start()
	}

	return func() {
		t.stop()
	}
}
