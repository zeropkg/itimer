package itimer

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/stretchr/testify/assert"
)

var f = func(seconds ...time.Duration) {
	var second time.Duration = 1
	if len(seconds) > 0 {
		second = seconds[0]
	}
	time.Sleep(time.Second * second)
}

func TestCommon(t *testing.T) {
	tt := newTimer()
	tt.start()
	f(1)
	tt.stop()
	// 大于等于1秒
	assert.GreaterOrEqual(t, tt.timeUsed, int64(1000))
}

func TestStartOnceMore(t *testing.T) {
	tt := newTimer()
	tt.start() // the first one should take effect
	f(1)
	tt.start() // take no effect
	f(2)
	tt.stop()
	// fmt.Println(tt.getTotalTime())
}

func TestStopOnceMore(t *testing.T) {
	tt := newTimer()
	tt.start()
	f(1)
	tt.stop() // the first one should take effect
	f(2)
	tt.stop() // take no effect
	fmt.Println(tt.timeUsed)
}

func demo1(tt *Timer) {
	tt.start()
	// ur logic ， async schedule and serialize the timer
	bytes, _ := json.Marshal(tt)
	f(1)                      // to mock the async feature compute time
	json.Unmarshal(bytes, tt) // 恢复现场，恢复计时器

	//tt.stop()
	defer tt.costTime()()
}

// 对于异步的情况需要业务方自己进行序列化和反序列，来记录时间
func TestAsyncCondition(t *testing.T) {
	tt := newTimer()
	demo1(tt)
	spew.Dump(tt)
}
